section .text

global exit
global string_length
global print_string
global print_newline
global print_char
global print_int
global print_uint
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global read_string
global print_err

%define stdin 0
%define stdout 1
%define stderr 2
%define ex 60
%define space 0x20
%define tab 0x9
%define newline 0xA
  
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, ex 
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
sub rdi, 1
xor rax, rax
sub rax, 1
.loop:
add rdi, 1
add rax, 1
mov rsi, [rdi]
cmp sil, 0x0
jne .loop
ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
  push    rdi
  call string_length
  pop     rsi
  mov     rdx, rax
  mov     rax, 1
  mov     rdi, stdout
  syscall
  ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, newline
; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rdx, 1
    mov rax, 1
    mov rdi, stdout
    syscall
    pop rax
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jge print_uint
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
; Выводит беззнаковое 8-байтовое число в десятичном формате 
print_uint:
    xor rdx, rdx
    mov r10, 10
    push rbp
    mov rbp, rsp
    mov rax, rdi
.floop:
    cmp rax, 10
    jb .one_digit
    div r10
.general:
    add rdx, '0'
    push rdx
    xor rdx, rdx
    cmp rax, 0
    jne .floop
    mov rcx, rbp
    sub rcx, rsp
    sar rcx, 3
    mov r9, rsp
    sub rsp, 16
    mov r8, rsp
.sloop:
    mov dil, [r9]
    mov byte[r8], dil
    add r9, 8
    inc r8
    dec rcx
    jnz .sloop
    mov byte[r8], 0
    mov rdi, rsp
    call print_string
    mov rsp, rbp
    pop rbp 
    ret

.one_digit:
    mov rdx, rax
    xor rax, rax
    jmp .general


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push rdi
    push rsi
    call string_length
    pop rdi
    push rdi
    push rax
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rax, rdx
    jne .ne
.loop:
    mov r8b, [rdi]
    mov r9b, [rsi]
    cmp r8b, r9b
    jne .ne
    cmp r9b, 0x0
    je .eq
    inc rsi
    inc rdi
    jmp .loop
.eq:
    mov rax, 1
    ret

.ne:
    xor rax, rax
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rdi, stdin
    mov rsi, rsp
    mov rdx, 1
    mov rax, 0
    syscall
    cmp rax, 0
    je .end
    pop rax
    ret
.end:
    pop rdi
    ret

; Принимает: адрес начала буфера, размер буфера

